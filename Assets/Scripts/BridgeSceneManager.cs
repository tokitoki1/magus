﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class BridgeSceneManager : MonoBehaviour {

  public void OnArmoryButtonPress()
  {
    SceneManager.LoadScene("Armory");
  }

	public void OnBattleButtonPress(){
    SceneManager.LoadScene("Game");
  }

	public void OnBackButtonPress(){
		SceneManager.LoadScene("MainMenu");
	}

}
