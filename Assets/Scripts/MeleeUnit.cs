﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeUnit : Unit {

	void Start () {
		anim = GetComponent<Animator>();
		isAttacking = false;
		maxHitPoints = hitPoints;
		targets = new List<GameObject>();
		SetSpriteOrder();
	}
	
	void Update () {
		Move();
		if (targets.Count == 0 && isAttacking) {
			anim.SetBool("isAttacking", false);
			isAttacking = false;
		}
	}
}
