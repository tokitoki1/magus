﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
	int damage = 10;
	float projectileSpeed = 10f;
	bool isEnemy = false;

	void Start () {
		
	}
	
	void Update () {
		Move();
	}

	void Move(){
		var movementVector = new Vector3(isEnemy ? -projectileSpeed * Time.deltaTime : projectileSpeed * Time.deltaTime, 0.0f, 0.0f);
      	transform.position += movementVector;
	}

	void OnCollisionEnter2D(Collision2D collision){
		if (collision.gameObject.GetComponent<Unit>() != null) {
			collision.gameObject.GetComponent<Unit>().TakeDamage(damage);
			Destroy(gameObject);
		}else{
			Debug.Log("The object that was collided with does not have a unit class");
		}
		// if (collision.gameObject.GetComponent<Unit>() != null) {
		// 	Debug.Log(collision.gameObject.GetComponent<Unit>());
		// 	if (collision.gameObject.GetComponent<Unit>().GetIsEnemy() != isEnemy){
		// 		Debug.Log("Is Enemy");
		// 		int currentHp = collision.gameObject.GetComponent<Unit>().TakeDamage(damage);
		// 		if (currentHp <= 0){
		// 			Destroy(collision.gameObject);
		// 		}
		// 		Destroy(gameObject);
    	// 	}
		// }
  	}

	public void SetDamage(int a) {
		damage = a;
	}

	public void SetSpeed(float a) {
		projectileSpeed = a;
	}

	public void SetIsEnemy(bool a) {
		isEnemy = a;
	}

}
