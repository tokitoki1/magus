﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{

  [SerializeField] public int hitPoints = 50;
  [SerializeField] public int baseDamage = 10;
  [SerializeField] public int cost = 0;
  [SerializeField] public float attackSpeed = 2f;
  [SerializeField] public float moveSpeed = 2f;
  [SerializeField] public bool isEnemy = false;
  [SerializeField] public AudioClip[] onAttackSounds;

  [Range(0f, 1f)] [SerializeField] public float attackVolume = 0.5f;


  // References
  protected Animator anim;
  protected AudioSource audioSource;

  // State
  protected bool isAttacking;
  protected int maxHitPoints;

  public List<GameObject> targets;

  // void Start () {
  // 	anim = GetComponent<Animator>();
  // 	isAttacking = false;
  // 	maxHitPoints = hitPoints;
  // 	targets = new List<GameObject>();
  // 	SetSpriteOrder();
  // }

  public void SetSpriteOrder()
  {
    // We want to order the units by Y value, the higher the Y value, the farther back they are on the screen (lower sortingOrder)
    // Since units are not moving up or down, we do not need this code inside of Update
    // https://answers.unity.com/questions/620318/sprite-layer-order-determined-by-y-value.html
    GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt(transform.position.y * 100f) * -1;
  }

  // void Update () {
  // 	Move();
  // 	//UpdateHealthBar();
  // 	if (targets.Count == 0 && isAttacking) {
  // 		anim.SetBool("isAttacking", false);
  // 		isAttacking = false;
  // 	}
  // }

  public void Move()
  {
    if (!isAttacking)
    {
      // If the unit is an enemy, move to the left
      var movementVector = new Vector3(isEnemy ? -moveSpeed * Time.deltaTime : moveSpeed * Time.deltaTime, 0.0f, 0.0f);
      transform.position += movementVector;
    }
  }

  public int TakeDamage(int damage)
  {
    hitPoints -= damage;
    UpdateHealthBar();
    if(hitPoints <= 0){
      Destroy(gameObject);
    }
    return hitPoints;
  }

  public void UpdateHealthBar()
  {
    Transform myHealthBar = gameObject.transform.GetChild(0); // Make sure health bar is index 0
    float index = (float)hitPoints * 10 / (float)maxHitPoints;
    myHealthBar.GetComponent<Animator>().SetInteger("hp", (int)Mathf.Ceil(index));
  }

  public void OnAttack()
  {
    //play the sound effect from the array of sounds
    if (onAttackSounds.Length != 0)
    {
      //Choose a random sound
      int soundIndex = Random.Range(0, onAttackSounds.Length);
      audioSource.PlayOneShot(onAttackSounds[soundIndex], attackVolume);
    }
    for (int x = 0; x < targets.Count; x++)
    {
      GameObject currentObj = targets[x];
      if (currentObj != null) {
        int currentHp = currentObj.GetComponent<Unit>().TakeDamage(baseDamage);
        if (currentHp <= 0)
        {
          targets.Remove(currentObj);
          // Destroy(currentObj);
        }
      }
      else {
        targets.Remove(currentObj);
      }
    }
  }

  public int GetCost()
  {
    return cost;
  }

  public bool GetIsEnemy()
  {
    return isEnemy;
  }

  void OnCollisionEnter2D(Collision2D collision)
  {
    if (collision.gameObject.GetComponent<Unit>() != null) {
      if (collision.gameObject.GetComponent<Unit>().GetIsEnemy() != isEnemy) {
        targets.Add(collision.gameObject);
        if (!isAttacking) {
          anim.SetBool("isAttacking", true);
          isAttacking = true;
        }
      }
    }
  }

  void OnCollisionExit2D(Collision2D collision)
  {
    targets.Remove(collision.gameObject);
  }

}
