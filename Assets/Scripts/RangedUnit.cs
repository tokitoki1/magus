﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedUnit : Unit {
    [SerializeField] public float Range;
    [SerializeField] public GameObject Projectile;
    [SerializeField] public GameObject ProjectileSpawnLocation;
	void Start () {
		anim = GetComponent<Animator>();
		isAttacking = false;
		maxHitPoints = hitPoints;
		targets = new List<GameObject>();
		SetSpriteOrder();
	}
	
	void Update () {
		Move();
        bool checkAttack = false;
        string unitType;
        if (isEnemy) {
            unitType = "Ally";
        }
        else {
            unitType = "Enemy";
        }
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(unitType);
        foreach(GameObject Enemy in enemies) {
            Vector3 difference = transform.position - Enemy.transform.position;
            float distanceSqr = difference.sqrMagnitude;
            if (distanceSqr < Range * Range) {
                //Within range
                checkAttack = true;
                anim.SetBool("isAttacking", true);
                isAttacking = true;
                break;
            }
        }
        if (!checkAttack) {
            anim.SetBool("isAttacking", false);
            isAttacking = false;
        }
	}

    public new void OnAttack(){
        //play the sound effect from the array of sounds
        if (onAttackSounds.Length != 0){
            //Choose a random sound
            int soundIndex = Random.Range(0, onAttackSounds.Length);
            audioSource.PlayOneShot(onAttackSounds[soundIndex], attackVolume);
        }
        var yOffset = transform.position.y + (gameObject.GetComponent<BoxCollider2D>().size.y / 2);
        GameObject proj = Instantiate(this.Projectile, ProjectileSpawnLocation.transform.position, Quaternion.identity) as GameObject;
        var obj = proj.GetComponent<Projectile>();
        obj.SetDamage(baseDamage);
        obj.SetSpeed(10f);
        obj.SetIsEnemy(isEnemy);
    }
}