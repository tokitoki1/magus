﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	[SerializeField] int level = 0;
	[SerializeField] int gold = 0;
	[SerializeField] int crystals = 0;

	MusicManager musicManager;

	private void Awake(){
		int gameManagerCount = FindObjectsOfType<GameManager>().Length;
		if(gameManagerCount > 1){
			Destroy(gameObject);
		}else{
			DontDestroyOnLoad(gameObject);
		}
	}

	void Start () {
		// Set music manager, play the main theme
		musicManager = FindObjectOfType<MusicManager>();
		musicManager.PlayMainTheme();

		// Load save data from files
	}
}
