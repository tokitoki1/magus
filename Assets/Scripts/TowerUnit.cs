﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerUnit : Unit {
	void Start () {
		maxHitPoints = hitPoints;
		SetSpriteOrder();
	}

	void Update () {

	}

	//Override for Collision
	void OnCollisionEnter2D(Collision2D collision){
	}
	
	//Override for Collision
	void OnCollisionExit2D(Collision2D collision) {

	}
}
