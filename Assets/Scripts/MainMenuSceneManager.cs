﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuSceneManager : MonoBehaviour {

  public void OnPlayPress()
  {
    SceneManager.LoadScene("Bridge");
  }

  public void OnQuitPress()
  {
    Application.Quit();
  }
}
