﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BattleManager : MonoBehaviour
{
  [SerializeField] int maxMana = 50;
  [SerializeField] int manaRegen = 1;
  [SerializeField] GameObject[] units;
  [SerializeField] Transform[] spawnPoints;
  [SerializeField] TextMeshProUGUI currentManaTag;

  int currentMana;
  int allyDeathCount;
  int enemyDeathCount;

  void Start()
  {
    // Stop the main menu music and start the battle music!
    FindObjectOfType<MusicManager>().PlayBattleTheme();

    currentMana = 0;
    allyDeathCount = 0;
    enemyDeathCount = 0;
    StartCoroutine(generateMana());
  }

  void Update()
  {

    if (Input.GetKeyDown("a"))
    {
      Spawn(4);
    }
    if (Input.GetKeyDown("d"))
    {
      Spawn(6);
    }
    if (Input.GetKeyDown("s"))
    {
      Spawn(3);
    }

  }

  IEnumerator generateMana()
  {
    while (true)
    {
      yield return new WaitForSeconds(1.0f);
      if (currentMana <= maxMana)
      {
        if ((currentMana + manaRegen) > maxMana)
        {
          currentMana = maxMana;
        }
        else
        {
          currentMana += manaRegen;
        }
        updateCurrentMana();
      }
      else
      {
        currentMana = maxMana;
      }
    }
  }

  void updateCurrentMana()
  {
    currentManaTag.SetText(currentMana.ToString());
  }

  public void Spawn(int unitId)
  {
    int spawnIndex = 0;
    if (units[unitId].GetComponent<Unit>().GetIsEnemy())
    {
      spawnIndex = 1;
    }
    else
    {
      //Check if there is enough mana
      int unitCost = units[unitId].GetComponent<Unit>().GetCost();
      if (currentMana < unitCost)
      {
        return;
      }
      else
      {
        currentMana -= unitCost;
        updateCurrentMana();
      }
    }
    //Randomize y from 0.4 to -0.2
    float x = spawnPoints[spawnIndex].position.x;
    float y = spawnPoints[spawnIndex].position.y + Random.Range(0, 1f);
    Instantiate(units[unitId], new Vector3(x, y, 0), Quaternion.identity);
  }


}
