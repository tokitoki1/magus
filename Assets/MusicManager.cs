﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

	[SerializeField] AudioSource MainTheme;
  [SerializeField] AudioSource BattleTheme;

  private void Awake()
  {
    int musicManagerCount = FindObjectsOfType<MusicManager>().Length;
    if (musicManagerCount > 1)
    {
      Destroy(gameObject);
    }
    else
    {
      DontDestroyOnLoad(gameObject);
    }
  }

  public void PlayMainTheme(){
		BattleTheme.Stop();
		MainTheme.Play();
	}

	public void PlayBattleTheme(){
		MainTheme.Stop();
		BattleTheme.Play();
	}
}
